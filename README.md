Changelogs ToPPazZe
```
15 Mars 2019
```
**Informations**
```
❖ Début du nouveau code de ToPPazZe repris de zéro.
❖ Préfix légèrement modifié. Avant "top!" | Maintenant "top."
```
**Commandes réalisées**
```
❖ Ping : top.ping
❖ Info Bot : top.ib
```
```
16 Mars 2019
```
**Informations**
```
❖ En plus du menu d'aide habituel, j'ai décidé de faire une commandes d'aide plus détaillée pour chacune des commandes de ToPPazZe.
```
**Commandes réalisées**
```
❖ Menu d'aide : top.help
Les commandes d'help supplémentaires, les top.infocmd, sont faites en même temps que la commande qu'elles accompagnent
❖ Guild Informations : top.si (A améliorer dans le futur, notamment avec une map des émojis et des rôles)
```
**Informations**
```
❖ Début du boulot sur la partie modération
```
**Commandes réalisées**
```
❖ Ban : top.ban
❖ Kick : top.kick
❖ Purge : top.purge
```